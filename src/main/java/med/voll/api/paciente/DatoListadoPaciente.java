package med.voll.api.paciente;

public record DatoListadoPaciente(String nombre, String email, String documentoIdentidad) {
    public DatoListadoPaciente(Paciente paciente) {
        this(paciente.getNombre(), paciente.getEmail(), paciente.getDocumentoIdentidad());
    }
}
