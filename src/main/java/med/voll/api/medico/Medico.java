package med.voll.api.medico;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import med.voll.api.direccion.Direccion;
@Table(name = "medicos")
@Entity(name = "Medico")
@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
public class Medico {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nombre;
    private String email;
    private String documento;
    private String telefono;
    @Enumerated(EnumType.STRING)
    private Especialidad especialidad;
    @Embedded
    private Direccion direccion;

    private Boolean activo;

    public Medico(DatosRegistroMedico datosRegistroMedico) {
        this.nombre = datosRegistroMedico.nombre();
        this.email = datosRegistroMedico.email();
        this.telefono = datosRegistroMedico.telefono();
        this.especialidad = datosRegistroMedico.especialidad();
        this.documento = datosRegistroMedico.documento();
        this.direccion = new Direccion(datosRegistroMedico.direccion());
        this.activo = true;
    }

    public void actualizarDatos(DatoActualizarMedico datoActualizarMedico) {
        if(datoActualizarMedico.nombre() != null){
            this.nombre = datoActualizarMedico.nombre();

        }
        if(datoActualizarMedico.documento() != null){
            this.documento = datoActualizarMedico.documento();
        }
        if(datoActualizarMedico.direccion()!=null){
            this.direccion = direccion.actualizarDatos(datoActualizarMedico.direccion());

        }

    }

    public void desactivarMedico() {
        this.activo = false;
    }
}
