package med.voll.api.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello") //para decir que ruta se sigue
public class HelloController {
    @GetMapping
    public String helloWorld(){
        return "hello, world";
    }
}
