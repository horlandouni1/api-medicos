CREATE TABLE medicos (
                         id bigserial PRIMARY KEY,
                         nombre varchar(100) NOT NULL,
                         email varchar(100) NOT NULL UNIQUE,
                         documento varchar(6) NOT NULL UNIQUE,
                         especialidad varchar(100) NOT NULL,
                         calle varchar(100) NOT NULL,
                         distrito varchar(100) NOT NULL,
                         complemento varchar(100),
                         numero varchar(20),
                         ciudad varchar(100) NOT NULL
);